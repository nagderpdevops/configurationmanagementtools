### source code is available in noman branch!
# What is Configuration Management?

Configuration management is all about automating significant and repetitive activities in an IT environment. Configuration management addresses tasks that scale to hundreds and thousands of machines.

Such tasks may include software installations, upgrades, and updates, patch management, security compliance, user management, among many others. There are configuration management tools to create and optimize run time environments.

Configuration management tools in DevOps provision required Infrastructure through scripts/ Infrastructure as Code.

We have several tools for these operations, here is the comparison of some popular tools.

![Comaprison Table](comparison_table.png)