# Ansible

Ansible automates Infrastructure configuration, application deployment, and cloud provisioning while leveraging the Infrastructure as Code service model.

This tool scales down repetitive tasks in infrastructure administration through defined playbooks. A playbook, in this case, is a simple YAML script file detailing the activities to be executed by the Ansible automation engine. With Ansible automation, the Operations team can create machine groups to be acted upon by defined tasks and control how machines run in production environments.

## Features

- Ansible Tower, a platform within Ansible, is a visualization dashboard for the entire IT environment.
- Through role-based access control (RBAC), Ansible scope can create users, and the permissions for the environments can operate.
- Ansible supports both On-premises and Multi-cloud Infrastructure configurations.