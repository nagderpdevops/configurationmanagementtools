# Puppet

Puppet is open-source platform suitable for provisioning resilient Infrastructure. Puppet can be used to configure, deploy, run servers and automate the deployment of the applications on the configured servers.

Through Puppet, it is possible to remediate the operational and security risks in an IT environment through Continuous compliance. It features windows infrastructure automation, patch management, and managed application operations.

## Feature 
- Highly extensible, supporting several developer tools and APIs.
- Puppet features Bolt, a powerful task orchestrator to automate manual tasks.
- Puppet integrates well with Kubernetes and Docker.