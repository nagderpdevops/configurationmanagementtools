# Saltstack

Saltstack or simply salt is a scalable configuration management and orchestration tool. We can use salt to manage IT environments such as data centers through event-driven orchestration and remote execution of configurations.

Salt configuration management framework relies on states and configuration files to show how an IT Infrastructure is provisioned and deployed. The configuration files describe Infrastructure packages to be installed, services to be started or stopped, users and user creation processes, and many other required tasks in provisioning an IT environment.

## Features

- Salt Cloud platform for provisioning systems on the Cloud.
- Supports both agent-based and agentless control of nodes.
- Supports both *NIX and Windows Operating systems.